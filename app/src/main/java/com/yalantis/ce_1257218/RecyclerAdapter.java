/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.yalantis.ce_1257218;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

/**
 * Created by Kotus on 12.03.2016.
 * Adapter of RecyclerView.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private String[] mDataset;
    private Context mContext;

    //Using ViewHolder class we get the link to each element of the separate list item
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView mImageView;
        private Context mContext;

        public ViewHolder(View view, Context context) {
            super(view);
            mImageView = (ImageView) view.findViewById(R.id.recycler_item);
            mContext = context;
            mImageView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(mContext, "Clicked on "+ getAdapterPosition() + " picture", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Defaul constructor of adapter
     *
     * @param dataset array with <b>Srting</b> urls
     * @param context <b>Activity</b>, which call this Class
     */
    public RecyclerAdapter(String[] dataset, Context context) {
        mDataset = dataset;
        mContext = context;
    }

    /**
    * Creates new views (calls layout`s manager)
    */
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        ViewHolder vh = new ViewHolder(v, mContext);
        return vh;

    }

    /**
     * Replace content of single View
     *
     * @param holder ViewHolder of image
     * @param position position in data array
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Picasso.with(mContext)
                .load(mDataset[position])
                .placeholder(R.drawable.ic_cached_24dp)
                .error(R.drawable.ic_cancel_24dp)
                .into(holder.mImageView);

    }

    /**
     * Returns data array size
     * @return <b>int</b> length of data array
     */
    @Override
    public int getItemCount() {
        return mDataset.length;
    }

}
